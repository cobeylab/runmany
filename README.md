# runmany: run many jobs

`runmany` lets you run a bunch of jobs in parallel, either on a single computer or on a SLURM cluster. You create a directory hierarchy where your jobs will run and a file in each directory describing the job; `runmany` executes the jobs locally or submits them to SLURM.

This should be pretty easy, but on the Midway cluster we're allowed more simultaneous processors (768) than SLURM submissions (48), so we have to bundle multiple single-processor jobs into multi-processor super-jobs (henceforth, “chunks”) that run in
parallel on a single node.

Additionally, `runmany` doesn't care how you organize your output. But if you organize your output so that each job generates a single SQLite databse, the included `gather` script will gather all the separate SQLite output databases into a single database, so you're left with a single file containing all your results, and you can delete all the working directories for individual jobs.

## Setup

First, get a copy of `runmany` on your local machine, using

```
cd wherever/you/want
git clone git@bitbucket.org:cobeylab/runmany.git
```

You'll find it convenient to have `runmany` and `gather` in your search path (so you can just type `runmany` and `gather`). We have it installed in `/project/cobey/runmany`, so you can just make sure that this is in your `~/.bash_profile`:

```
PATH=/project/cobey/runmany:[...other things...]:$PATH
```

and the equivalent on your local machine.

## A Complete Example

This example is implemented in both Python in `example/python` and in R in `example/R`.
As the output is in SQLite format (not a requirement), it's useful to have a [graphical SQLite browser](https://github.com/sqlitebrowser/sqlitebrowser/releases), especially if you're not used to SQLite.

If you're following along in R, you'll need to install the `rjson`, `DBI`, and `RSQLite` packages.

If you're not familiar with JSON, it's just a simple format for representing arbitary hierarchical data; the [Wikipedia page](https://en.wikipedia.org/wiki/JSON) should be sufficient; the example code shows you how to load/dump JSON in R and Python.

### The Simulation

Say you have a stochastic simulation that takes two parameters, `alpha` and `beta`, in
addition to a random seed. You want to sweep over `alpha = [0.1, 0.2, 0.3]` and
`beta = [0.1, 0.4, 0.9]`, with 20 replicates per parameter combination.

First, you write a simulation script (`run_simulation.py` or `run_simulation.R`) that
loads a `parameters.json` file from the working directory, which will look something like
this:

```{json}
{
  "alpha": 0.1, 
  "beta": 0.1, 
  "replicate_id": 4, 
  "random_seed": 108368064
}
```

Note that `runmany` does not care how you format the input for your simulation; it just
matters that your simulation script can load its parameters from the working directory.

### The Parameter Sweep

Then, you write a script (`generate_jobs.py` or `generate_jobs.R`) that creates a
directory hierarchy of jobs, with the top-level directories named with parameter values
and the inner directories, which will be job working directories, named for the replicate
number:

```
jobs/
    alpha=0.1-beta=0.1/
        00/
        ...
        19/
    ...
    alpha=0.3-beta=0.9/
        00/
        ...
        19/
```

Also note that `runmany` does not care how you organize your directory hierarchy,
as long as there is a `runmany_info.json` file in each job's working directory.

Both `generate_jobs.py` and `generate_jobs.R` are written so that you can run them directly from the Unix command line like so:

```{sh}
./generate_jobs.py
./generate_jobs.R
```

Try it, and see what gets created. As you'd expect, there needs to be a `parameters.json` file in each working directory, so that a job running there can load its parameters.

Additionally, `runmany` requires a `runmany_info.json` file in each working directory, so that it knows what command to run, and, optionally, how long it will take and how much memory it will use. look like the following:

```{json}
{
  "executable": "/Users/ebaskerv/uchicago/runmany/example/python/run_simulation.py", 
  "arguments": [
    "--seasonal", 
    "--optimized"
  ], 
  "environment": {
    "BLAHBLAH": "blahblahvalue"
  }, 
  "minutes": 0.1, 
  "megabytes": 3000
}
```

The only required piece is `executable`: this tells `runmany` what to run in the directory. The `arguments` and `environment` are shown here for illustrative purposes; they allow you to pass command-line arguments to the executable and set environment variables.

The last two keys, `minutes` and `megabytes`, are used as hints for constructing SLURM tasks. They should be overestimates of how long your jobs will take and how much memory they will use. `runmany` will make sure to allocate few enough jobs in the chunk so that SLURM limits are not exceeded.

### Running Locally

To run all your jobs on your local machine, just run
```{sh}
runmany local jobs --cores 4
```
from the `example/R` or `example/python` directory after you've generated the jobs.

On Mac OS X, you can leave off `--cores 4` and it will automatically detect the number of physical cores on your machine. (On Linux, it currently makes an educated guess but until I add the ability to do it for real you should just specify.)

If everything completes successfully, there are now some extra files in every working directory:
```
output.sqlite
runmany_status.json
```

The `output.sqlite` file was created by the simulation, and contains our results. `runmany_status.json` is used to track the status of individual jobs. If your jobs produce output on standard output and/or standard error, there will additionally be `stdout.txt` and/or `stderr.txt` files, respectively.

WARNING: THIS MEANS THAT IF YOU HAVE 8000 JOBS THERE MIGHT BE 24000 EXTRA FILES YOU DIDN'T PLAN ON CREATING. REMEMBER TO DELETE THEM WHEN YOU'RE DONE.

Take a look at the `output.sqlite` files in the SQLite browser. You'll see a `parameters` table with `alpha`, `beta`, `replicate_id` and `random_seed` columns; and you'll see a `results` table with the (meaningless) simulation output (with `time` and `value` columns).

Then, run

```{sh}
gather jobs output_gathered.sqlite
```

and compare the gathered database to the original files. You'll see an extra table, `job_index`, which maps job identifiers to database files. Additionally, in every table you'll see an extra column, `job_id`, that contains the job identifier corresponding to the database that the rows came from.

### Running on Midway

Before running the example on Midway, copy it from `/project/cobey/example/{R,python}` to your home directory:

```{sh}
cd ~
cp -r /project/cobey/runmany/example/python ./runmany-example
cd runmany-example
```

Then run generate jobs just as before:

```{sh}
./generate_jobs.py
./generate_jobs.R
```

Finally, let's test constructing the SLURM jobs without actually submitting them:

```{sh}
runmany slurm myexp jobs chunks --chunks 10 --dry
```

This sets up an experiment whose SLURM tasks will have the prefix `myexp`, whose jobs are located in `jobs`, and whose "chunks" (collections of jobs to be submitted together to SLURM) will be set up in the `chunks` directory.

The `--dry` argument specifies a "dry run", which creates an `.sbatch` file for each chunk and shows what the `sbatch` command would be, but does not actually run `sbatch`. (You can perform dry runs on your local machine without any trouble, too, even though `sbatch` isn't present.)

The argument `--chunks 10` is optional and instructs `runmany` to only submit a total of 10 chunks if possible, unless time limits are exceeded. By default `--chunks` is set to 48, which is the maximum number of simultaneous SLURM tasks allowed on Midway.

If things are set up properly, you should see:

```{sh}
Identified 180 job directories.
Running jobs in 10 SLURM chunks.
```

and 10 `sbatch` commands and their results. You'll notice that each directory in `chunks` contains two files:
* `chunk_info.json` specifies how many cores and what jobs are allocated to the chunk, which should have correctly calculated how long, and how much memory, each chunk needs.
* `run_chunk.sbatch` is a SLURM sbatch file for the chunk.

Now, let's remove our dry-run chunks and submit for real:

```{sh}
rm -r chunks
runmany slurm myexp jobs chunks --chunks 10
```

If you don't have anything else running on Midway, the whole thing should be done rather quickly. You'll now notice two additional files in each `chunk` directory:
* `sbatch_out.txt` is the output of the `sbatch` file;
* and `stderr.txt` and `stdout.txt` contain the output from the actual chunk script, which is akin to the output from a single `runmany local` invocation.

If you made a mistake, you can run

```{sh}
runmany scancel chunks
```

to cancel the SLURM tasks associated with all the chunks.

Then, as before, you can run

```{sh}
gather jobs output_gathered.sqlite
```

## Checking job status

To check jobs status, use `runmany status`:

```{sh}
runmany status jobs-dir
```

This will tell you how many jobs are waiting to run (`waiting`); how many are currently running (`running`); how many completed successfully with status code 0 (`complete`), and how many completed with a non-zero status code (`failed`).

To list all the directories associated with a particular state, use  `-l`, `-L`, or `--list-status`, e.g.

```{sh}
runmany status jobs-dir -l failed
```

To repeatedly check status, use `-i`, `-I`, or `--iterate` followed by the number of seconds you want to wait between checks:

```{sh}
runmany status jobs-dir -i 5
```

## SLURM options and hints

You can modify how `runmany slurm` creates SLURM tasks by giving it additional command-line options:

* `--cores-per-chunk 8` will limit the number of cores used per chunk to 8; this number must be less than the number available on a single node (default 16).
* `--chunks 24` will try to allocate 24 chunks total (default 48), so only 24 of your 48 allocated jobs will be used by this experiment

## What runmany doesn't do

* Provide a general way to do parameter sweeps. I used to write scripts that did this, but it's actually easier to write for loops for each experiment.
* Impose any restrictions on how you organize your jobs, aside from that pesky `runmany_info.json` file.
* Have a graphical user interface. Someday computational science will find its way into the 1980s, I hope.

## What gather doesn't do

* Add SQLite indexes to the gathered database. Before running queries on large databases, make sure you add appropriate indexes to make the queries fast.

## Other warnings

* DELETE YOUR FILES when done with them. `gather` makes it easy to put everything into a single file, so take advantage of that fact.

