#!/usr/bin/env python

import os
import sys
import sqlite3
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--no-job-ids', action='store_true',
        help='Do not add a job_id column to database tables.'
    )
    parser.add_argument(
        '--no-job-index', action='store_true',
        help='Do not create a job_index table containing source directories.'
    )
    parser.add_argument(
        '--filename',
        default=None,
        help='Only load databases with this filename.'
    )
    parser.add_argument(
        '--extension',
        default='.sqlite',
        help='Only load databases with this extension.'
    )
    parser.add_argument(
        '--exclude-table',
        metavar='<table>',
        help='Exclude database table. Multiple tables can be specified with --append <table1> --append <table2> ...',
        action='append'
    )
    parser.add_argument(
        'jobs_dir', metavar='<jobs-directory>',
        help='Directory containing databases to be gathered.'
    )
    parser.add_argument(
        'output_db_path', metavar='<output-database-path>',
        help='Path to the gathered output database to be created.'
    )
    args = parser.parse_args()
    if not args.extension.startswith('.'):
        args.extension = '.' + args.extension
    
    if os.path.exists(args.output_db_path):
        sys.stderr.write('Output database {} already exists; aborting.\n'.format(args.output_db_path))
        sys.exit(1)
    
    db_paths = find_database_paths(args)
    
    out_db = sqlite3.connect(args.output_db_path)
    
    if not (args.no_job_ids or args.no_job_index):
        try:
            out_db.execute('CREATE TABLE job_index (job_id, db_path)')
        except:
            sys.stderr.write('Could not create job_index table; aborting.\n')
    
    job_id = 0
    for db_path in db_paths:
        sys.stderr.write('{}\n'.format(db_path))
        
        job_db = sqlite3.connect(db_path)
        
        if not (args.no_job_ids or args.no_job_index):
            out_db.execute('INSERT INTO job_index VALUES (?, ?)', [job_id, db_path])
            out_db.commit()
        
        for table_name, create_sql in job_db.execute('SELECT name, sql FROM sqlite_master WHERE type = "table"'):
            if args.exclude_table is not None and table_name in args.exclude_table:
                continue
            
            # Create table if it doesn't exist
            if args.no_job_ids:
                colnames = []
            else:
                colnames = ['job_id']
            colnames += [x[0] for x in job_db.execute('SELECT * FROM {}'.format(table_name)).description]
            out_db.execute(
                'CREATE TABLE IF NOT EXISTS {} ({})'.format(table_name, ','.join(colnames))
            )

            # Insert all rows into master database
            for row in job_db.execute('SELECT * FROM {}'.format(table_name)):
                if args.no_job_ids:
                    values = row
                else:
                    values = [job_id] + list(row)
                out_db.execute(
                    'INSERT INTO {} VALUES ({})'.format(
                        table_name, ','.join(['?'] * len(colnames))
                    ), values
                )
            out_db.commit()
    
        job_db.close()
        job_id += 1
    
    out_db.close()

def find_database_paths(args):
    paths = [args.jobs_dir]
    
    db_paths = []
    
    def process_file(filename):
        use_file = False
        
        basename = os.path.basename(filename)
        prefix, ext = os.path.splitext(basename)
        
        if args.filename is not None:
            if args.filename.endswith(args.extension):
                if basename == args.filename:
                    use_file = True
            else:
                if prefix == args.filename and ext == args.extension:
                    use_file = True
        else:
            if ext == args.extension:
                use_file = True
        
        if use_file:
            db_paths.append(os.path.abspath(filename))
    
    for path in paths:
        if os.path.isfile(path):
            process_file(filename)
        elif os.path.isdir(path):
            for root, dirs, files in os.walk(path):
                dirs.sort()
                for filename in files:
                    process_file(os.path.join(root, filename))
    
    return db_paths
            

if __name__ == '__main__':
    main()
