#!/usr/bin/env python

import os
import sys
import subprocess
import multiprocessing
import time
from datetime import datetime
import json
import math
import traceback
import argparse
from collections import OrderedDict
from collections import Counter

INFO_FILENAME = 'runmany_info.json'
STATUS_FILENAME = 'runmany_status.json'

def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='command')

    local_parser = subparsers.add_parser('local')
    local_parser.add_argument('--dry', action='store_true')
    local_parser.add_argument('--cores', metavar='<cores>', default='detect')
    local_parser.add_argument('jobs_dir', metavar='<jobs-directory>')
    
    slurm_parser = subparsers.add_parser('slurm')
    slurm_parser.add_argument('--dry', action='store_true')
    slurm_parser.add_argument('--cores-per-chunk', metavar='<cores-per-chunk>', type=int, default=16)
    slurm_parser.add_argument('--chunks', metavar='<ideal-chunk-count>', type=int, default=48)
    slurm_parser.add_argument('--max-chunks', metavar='<max-chunk-count>', type=int, default=500)
    slurm_parser.add_argument('--max-chunk-minutes', metavar='<max-chunk-minutes>', type=float, default=2160)
    slurm_parser.add_argument('--default-job-megabytes', metavar='<default-job-megabytes>', type=float, default=2000)
    slurm_parser.add_argument('--max-chunk-megabytes', metavar='<max-chunk-megabytes>', type=float, default=32000)
    slurm_parser.add_argument('job_name_prefix', metavar='<job-name-prefix>')
    slurm_parser.add_argument('jobs_dir', metavar='<jobs-directory>')
    slurm_parser.add_argument('chunks_dir', metavar='<chunks-directory>')
    
    scancel_parser = subparsers.add_parser('scancel')
    scancel_parser.add_argument('chunks_dir', metavar='<chunks-directory>')
    
    status_parser = subparsers.add_parser('status')
    status_parser.add_argument('-l', '-L', '--list-status', type=str, default=None, help='List jobs in this state')
    status_parser.add_argument('-i', '-I', '--iterate', metavar='<seconds>', type=float, default=None, help='Iterate every <seconds> seconds.')
    status_parser.add_argument('jobs_dir', metavar='<jobs-directory>')
    
    reset_parser = subparsers.add_parser('reset')
    reset_parser.add_argument('-s', '-S', '--status', type=str, default=None, help='Reset jobs with this status')
    reset_parser.add_argument('jobs_dir', metavar='<jobs-directory>')
    
    chunk_parser = subparsers.add_parser('chunk')

    args, unknown_args = parser.parse_known_args()

    if args.command == 'local':
        runmany_local(args, unknown_args)
    elif args.command == 'slurm':
        runmany_slurm(args, unknown_args)
    elif args.command == 'scancel':
        runmany_scancel(args, unknown_args)
    elif args.command == 'chunk':
        runmany_chunk(args, unknown_args)
    elif args.command == 'status':
        runmany_status(args, unknown_args)
    elif args.command == 'reset':
        runmany_reset(args, unknown_args)

def runmany_local(args, unknown_args):
    if not os.path.exists(args.jobs_dir):
        sys.stderr.write('{} does not exist; aborting.\n'.format(args.jobs_dir))
        sys.exit(1)

    # Parse number of cores
    if args.cores == 'detect':
        try:
            cores = detect_core_count()
            sys.stderr.write('Using all {} physical cores detected.\n'.format(cores))
        except:
            try:
                cores = multiprocessing.cpu_count() / 2
                sys.stderr.write('Could not detect number of physical cores; using (logical cores detected)/2 = {}\n'.format(cores))
            except:
                cores = 1
                sys.stderr.write('Could not get detect number of cores; using 1 core.\n')
    else:
        try:
            cores = int(args.cores)
            sys.stderr.write('Using {} cores.\n'.format(cores))
        except:
            sys.stderr.write('Could not parse number of cores {}. Aborting.\n'.format(args.cores))
            sys.exit(1)

    # Find jobs to run
    job_dirs = identify_job_dirs(args)
    run_jobs(job_dirs, None, cores)

def runmany_slurm(args, unknown_args):
    if not os.path.exists(args.jobs_dir):
        sys.stderr.write('{} does not exist; aborting.\n'.format(args.jobs_dir))
        sys.exit(1)
    if os.path.exists(args.chunks_dir):
        sys.stderr.write('{} already exists; aborting.\n'.format(args.chunks_dir))
        sys.exit(1)

    # Find jobs to run
    job_dirs = identify_job_dirs(args)

    # Find maximum job time (in minutes) and size (in megabytes)
    max_job_minutes = None
    max_job_megabytes = None
    for job_dir in job_dirs:
        info_dict = load_json(os.path.join(job_dir, INFO_FILENAME))
        if 'minutes' in info_dict:
            if max_job_minutes is None or float(info_dict['minutes']) > max_job_minutes:
                max_job_minutes = float(info_dict['minutes'])
        if 'megabytes' in info_dict:
            if max_job_megabytes is None or float(info_dict['megabytes']) > max_job_megabytes:
                max_job_megabytes = float(info_dict['megabytes'])

    class Chunk:
        def __init__(self):
            self.job_dirs = []
            self.max_job_minutes = None
            self.max_job_megabytes = None

        def add_job(self, job_dir, minutes, megabytes):
            old_max_job_minutes = self.max_job_minutes
            old_max_job_megabytes = self.max_job_megabytes

            self.job_dirs.append(job_dir)
            if self.max_job_minutes is None or minutes > self.max_job_minutes:
                self.max_job_minutes = minutes
            if self.max_job_megabytes is None or megabytes > self.max_job_megabytes:
                self.max_job_megabytes = megabytes

            if self.minutes > args.max_chunk_minutes or self.megabytes > args.max_chunk_megabytes:
                print 'couldnt add', self.minutes, self.megabytes
                del self.job_dirs[-1]
                self.max_job_minutes = old_max_job_minutes
                self.max_job_megabytes = old_max_job_megabytes
                return False
            return True

        def get_n_jobs(self):
            return len(self.job_dirs)
        n_jobs = property(get_n_jobs)

        def get_megabytes(self):
            return self.cores * self.max_job_megabytes
        megabytes = property(get_megabytes)

        def get_cores(self):
            assert self.n_jobs > 0
            cores = max(1, min(
                self.n_jobs,
                int(math.floor(args.max_chunk_megabytes / self.max_job_megabytes)),
                args.cores_per_chunk
            ))
            return cores
        cores = property(get_cores)

        def get_max_cores(self):
            return min(int(math.floor(args.max_chunk_megabytes / self.max_job_megabytes)), args.cores_per_chunk)
        max_cores = property(get_max_cores)

        def get_minutes_for_cores(self, cores):
            return math.ceil(float(self.n_jobs) / cores) * self.max_job_minutes

        def get_minutes(self):
            return self.get_minutes_for_cores(self.cores)
        minutes = property(get_minutes)

    chunks = []

    def add_to_new_chunk_if_chunk_count_below_ideal(job_dir, minutes, megabytes):
        if len(chunks) < args.chunks:
            chunks.append(Chunk())
            if chunks[-1].add_job(job_dir, minutes, megabytes):
                return True
            else:
                sys.stderr.write('Could not add job {} to empty chunk; aborting.\n'.format(job_dir))
                sys.exit(1)
        return False

    def add_to_existing_chunk_if_possible(job_dir, minutes, megabytes):
        min_chunk = None
        for chunk in chunks:
            if min_chunk is None or chunk.minutes < min_chunk.minutes:
                min_chunk = chunk
            elif chunk.minutes == min_chunk.minutes and chunk.cores < min_chunk.cores:
                min_chunk = chunk

        return min_chunk.add_job(job_dir, minutes, megabytes)

    def add_to_new_chunk_if_possible(job_dir, minutes, megabytes):
        if len(chunks) < args.max_chunk_count:
            chunks.append(Chunk())
            if chunks[-1].add_job(job_dir, minutes, megabytes):
                return True
            else:
                sys.stderr.write('Could not add job {} to empty chunk; aborting.\n'.format(job_dir))
                sys.exit(1)
        return False

    for job_dir in job_dirs:
        info_dict = load_json(os.path.join(job_dir, INFO_FILENAME))

        if 'minutes' in info_dict:
            minutes = float(info_dict['minutes'])
        elif max_job_minutes is None:
            minutes = args.default_job_minutes
        else:
            minutes = max_job_minutes

        if 'megabytes' in info_dict:
            megabytes = float(info_dict['megabytes'])
        elif max_job_megabytes is None:
            megabytes = args.default_job_megabytes
        else:
            megabytes = max_job_megabytes

        if len(chunks) == 0:
            chunks.append(Chunk())
            if not chunks[0].add_job(job_dir, minutes, megabytes):
                sys.stderr.write('Could not add first job; aborting.\n')
                sys.exit(1)
        elif add_to_new_chunk_if_chunk_count_below_ideal(job_dir, minutes, megabytes):
            pass
        elif add_to_existing_chunk_if_possible(job_dir, minutes, megabytes):
            pass
        elif add_to_new_chunk_if_possible(job_dir, minutes, megabytes):
            pass
        else:
            sys.stderr.write('Could not fit jobs into limits. Aborting.\n')
            sys.exit(1)

    # Write out chunk info
    sys.stderr.write('Running jobs in {} SLURM chunks.\n'.format(len(chunks)))
    for chunk_id, chunk in enumerate(chunks):
        chunk_dir = os.path.join(args.chunks_dir, str(chunk_id))
        os.makedirs(chunk_dir)

        info_dict = OrderedDict([
            ('chunk_id', chunk_id),
            ('cores', chunk.cores),
            ('minutes', chunk.minutes),
            ('megabytes', chunk.megabytes),
            ('job_dirs', chunk.job_dirs)
        ])
        dump_json(info_dict, os.path.join(chunk_dir, 'chunk_info.json'))

        sbatch_filename = os.path.join(chunk_dir, 'run_chunk.sbatch')
        with open(sbatch_filename, 'w') as f:
            f.write('#!/bin/sh\n')
            for unknown_arg in unknown_args:
                f.write('#SBATCH {}\n'.format(unknown_arg))

            f.write('#SBATCH --ntasks=1\n')
            f.write('#SBATCH --cpus-per-task={}\n'.format(chunk.cores))
            f.write('#SBATCH --job-name={}-{}\n'.format(args.job_name_prefix, chunk_id))
            f.write('#SBATCH --output=stdout.txt\n')
            f.write('#SBATCH --error=stderr.txt\n')
            f.write('#SBATCH --time={}:00\n'.format(int(math.ceil(chunk.minutes))))
            f.write('#SBATCH --mem={}\n'.format(int(math.ceil(chunk.megabytes))))
            f.write('#SBATCH --parsable\n')
            f.write('\n')
            f.write('{} chunk\n'.format(os.path.abspath(__file__)))

            f.write('\n')

        proc_args = ['sbatch', os.path.abspath(sbatch_filename)]
        sys.stderr.write(' '.join(proc_args))
        sys.stderr.write('\n')

        if not args.dry:
            try:
                sbatch_out = subprocess.check_output(
                    proc_args,
                    cwd=chunk_dir
                )
                sys.stderr.write('{}'.format(sbatch_out))
                with open(os.path.join(chunk_dir, 'slurm_job_id.txt'), 'w') as f:
                    f.write(sbatch_out)
            except:
                sys.stderr.write('Could not execute sbatch\n')

def runmany_scancel(args, unknown_args):
    for subdir in os.listdir(args.chunks_dir):
        slurm_job_id_filename = os.path.join(args.chunks_dir, subdir, 'slurm_job_id.txt')
        if os.path.exists(slurm_job_id_filename):
            try:
                with open(slurm_job_id_filename) as f:
                    slurm_job_id = int(f.read())
                sys.stderr.write('chunk {}: slurm job ID {}\n'.format(subdir, slurm_job_id))
                sys.stderr.write('scancel {}\n'.format(slurm_job_id))
                subprocess.Popen(
                    ['scancel', '{}'.format(slurm_job_id)]
                ).wait()
            except:
                sys.stderr.write('An error occurred trying to cancel chunk {}\n'.format(subdir))

def runmany_chunk(args, unknown_args):
    info_dict = load_json('chunk_info.json')
    run_jobs(info_dict['job_dirs'], {'chunk_id' : info_dict['chunk_id']}, info_dict['cores'])

def runmany_status(args, unknown_args):
    jobs_dir = args.jobs_dir

    while True:
        counts = Counter()
        for root, dirs, files in os.walk(jobs_dir):
            if not os.path.exists(os.path.join(root, 'runmany_info.json')):
                continue
            status_obj = None
            try:
                status_filename = os.path.join(root, 'runmany_status.json')
                if os.path.exists(status_filename):
                    status_obj = load_json(status_filename)
                    status = status_obj['status']
                else:
                    status = 'waiting'
            except Exception as e:
                sys.stderr.write('Got exception checking job in\n{}\n{}\n'.format(root, e))
                status = 'unknown'

            if args.list_status == status:
                json.dump(status_obj, sys.stdout, indent=2)
                sys.stdout.write('\n')

            counts[status] += 1

        json.dump(counts, sys.stdout, indent=2)
        sys.stdout.write('\n')

        if args.iterate is None:
            break
        time.sleep(args.iterate)

def runmany_reset(args, unknown_args):
    jobs_dir = args.jobs_dir

    for root, dirs, files in os.walk(jobs_dir):
        if not os.path.exists(os.path.join(root, 'runmany_info.json')):
            continue
        status_obj = None
        try:
            status_filename = os.path.join(root, 'runmany_status.json')
            if os.path.exists(status_filename):
                status_obj = load_json(status_filename)
                status = status_obj['status']
                if (args.status is None) or (args.status == status):
                    os.remove(status_filename)
        except Exception as e:
            sys.stderr.write('Got exception resetting job in\n{}\n{}\n'.format(root, e))


def identify_job_dirs(args):
    job_dirs = []
    for root, dirs, files in os.walk(args.jobs_dir):
        dirs.sort()

        job_dir = os.path.abspath(root)
        info_filename = os.path.join(job_dir, INFO_FILENAME)
        status_filename = os.path.join(job_dir, STATUS_FILENAME)
        if os.path.exists(info_filename) and not os.path.exists(status_filename):
            job_dirs.append(job_dir)
    sys.stderr.write('Identified {} job directories.\n'.format(len(job_dirs)))

    return job_dirs


def run_jobs(job_dirs, info_dict, cores):
    pool = multiprocessing.Pool(cores)
    run_job_args = [(job_dir, info_dict) for job_dir in job_dirs]
    async_result = pool.map_async(run_job, run_job_args, chunksize=1)
    
    try:
        # Need a timeout to avoid KeyboardInterrupt Python bug; a million years should be safe
        async_result.get(60*60*24*365*1000*1000)
    except KeyboardInterrupt:
        pool.terminate()
        pool.join()
        sys.stdout.write('\n')

def run_job(args):
    job_dir, aux_info_dict = args
    
    start_time = datetime.utcnow()
    def write_status(status, code=None, error_message=None, end_time=None):
        status_dict = OrderedDict([
            ('job_dir', job_dir),
            ('status', status),
            ('start_time', '{}'.format(start_time))
        ])
        if aux_info_dict is not None:
            status_dict.update(aux_info_dict)
        
        if end_time is not None:
            status_dict['end_time'] = '{}'.format(end_time)
        if code is not None:
            status_dict['code'] = code
        if error_message is not None:
            status_dict['error_message'] = error_message
        dump_json(status_dict, os.path.join(job_dir, STATUS_FILENAME))

    sys.stderr.write('{} : starting job:\n  {}\n'.format(start_time, job_dir))

    write_status('running')
    try:
        info_dict = load_json(os.path.join(job_dir, INFO_FILENAME))
        exec_path = info_dict['executable']

        if 'arguments' in info_dict:
            exec_args = info_dict['arguments']
        else:
            exec_args = []

        env = dict(os.environ)
        if 'environment' in info_dict:
            env.update(info_dict['environment'])

        if not os.path.exists(exec_path):
            end_time = datetime.utcnow()
            sys.stderr.write('{} : job failed:\n  {}\n  executable {} not present\n'.format(end_time, job_dir, exec_path))
            write_status('failed', error_message='executable not present')
            return

        exec_and_args = [exec_path] + exec_args
        stdout_filename = os.path.join(job_dir, 'stdout.txt')
        stdout = open(stdout_filename, 'w')
        stderr_filename = os.path.join(job_dir, 'stderr.txt')
        stderr = open(stderr_filename, 'w')
        proc = subprocess.Popen(
            exec_and_args,
            cwd=job_dir,
            env=env,
            stdout=stdout,
            stderr=stderr
        )
        code = proc.wait()
        stdout.close()
        stderr.close()

        if os.path.getsize(stdout_filename) == 0:
            os.remove(stdout_filename)
        if os.path.getsize(stderr_filename) == 0:
            os.remove(stderr_filename)

        end_time = datetime.utcnow()
        sys.stderr.write(
            '{} : job complete with status code {}:\n  {}\n'.format(end_time, code, job_dir)
        )
        if code == 0:
            write_status('complete', code=code, end_time=end_time)
        else:
            write_status('failed', code=code, end_time=end_time)
            with open(os.path.join(job_dir, 'stderr.txt')) as f:
                stderr_data = f.read()
            sys.stderr.write('error:\n{}\n{}\n'.format(job_dir, stderr_data))
    except KeyboardInterrupt:
        pass
    except Exception as e:
        type, value, tb = sys.exc_info()
        exception_lines = traceback.format_exception(type, value, tb)
        end_time = datetime.utcnow()
        sys.stderr.write('{} : Exception trying to run process:\n'.format(end_time))
        for line in exception_lines:
            sys.stderr.write('  ' + line)
        write_status('failed', error_message=''.join(exception_lines), end_time=end_time)


def load_json(filename):
    with open(filename) as f:
        return json.load(f, object_pairs_hook=OrderedDict)

def dump_json(obj, filename):
    with open(filename, 'w') as f:
        json.dump(obj, f, indent=2)
        f.write('\n')

def detect_core_count():
    if sys.platform == 'darwin':
        return detect_core_count_darwin()
    raise Exception('unsupported platform')

def detect_core_count_darwin():
    return int(subprocess.check_output(['sysctl', '-n', 'hw.physicalcpu']))

if __name__ == '__main__':
    main()

