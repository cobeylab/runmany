#!/usr/bin/env python

import os
SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))
import sys
import json
import sqlite3
import numpy
from collections import OrderedDict

def main():
    parameters = load_json('parameters.json')
    rng = numpy.random.RandomState(parameters['random_seed'])
    
    results = rng.beta(parameters['alpha'], parameters['beta'], size=100)
    
    assert not os.path.exists('output.sqlite')
    output_db = sqlite3.connect('output.sqlite')
    
    output_db.execute('CREATE TABLE parameters (alpha, beta, replicate_id, random_seed)')
    output_db.execute(
        'INSERT INTO parameters VALUES (?,?,?,?)',
        [parameters[key] for key in ['alpha', 'beta', 'replicate_id', 'random_seed']]
    )
    
    output_db.execute('CREATE TABLE results (time, value)')
    for time, value in enumerate(results):
        output_db.execute('INSERT INTO results VALUES (?, ?)', [time, value])
    
    output_db.commit()
    output_db.close()
    
    #sys.stdout.write('this is standard output\n')
    #sys.stderr.write('this is standard error\n')

def load_json(filename):
    with open(filename) as f:
        return json.load(f, object_pairs_hook=OrderedDict)

if __name__ == '__main__':
    main()
