#!/usr/bin/env python

import os
SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))
import sys
import json
import random
from collections import OrderedDict

N_REPLICATES = 20
JOBS_DIR = os.path.join(SCRIPT_DIR, 'jobs')

def main():
    seed_rng = random.SystemRandom()
    
    if os.path.exists(JOBS_DIR):
        sys.stderr.write('{} already exists; aborting.\n'.format(JOBS_DIR))
        sys.exit(1)

    # Create job directories however you like.
    # It doesn't matter how the jobs are organized into subdirectories as long as
    # there's a runmany_info.json file in each directory that a job should run in.
    for alpha in [0.1, 0.2, 0.3]:
        for beta in [0.1, 0.4, 0.9]:
            for replicate_id in range(N_REPLICATES):
                job_dir = os.path.join(
                    JOBS_DIR,
                    'alpha={:.2g}-beta={:.2g}'.format(alpha, beta),
                    '{:02d}'.format(replicate_id)
                )
                sys.stderr.write('{}\n'.format(job_dir))
                os.makedirs(job_dir)
            
                # Make the runmany_info.json file with job info.
                # executable should be an absolute path to the job script/program.
                # arguments (optional) is a list of command-line arguments to be passed to the executable.
                # environment (optional) is a dictionary of environment variables to be set for the job.
                # minutes (optional) is an upper bound on job runtime in minutes.
                # megabytes (optional) is an upper bound on job memory usage in megabytes.
                # The working directory for the job will always be the directory containing
                # the runmany_info.json file.
                dump_json(
                    OrderedDict([
                        ('executable', os.path.join(SCRIPT_DIR, 'run_simulation.py')),
                        ('arguments', ['--seasonal', '--optimized']),
                        ('environment', {'BLAHBLAH' : 'blahblahvalue'}),
                        ('minutes', 0.1),
                        ('megabytes', 100)
                    ]),
                    os.path.join(job_dir, 'runmany_info.json')
                )
                
                # Make additional files for your job if you like, e.g., containing parameter values.
                dump_json(
                    OrderedDict([
                        ('alpha', alpha),
                        ('beta', beta),
                        ('replicate_id', replicate_id),
                        ('random_seed', seed_rng.randint(1, 2**31-1))
                    ]),
                    os.path.join(job_dir, 'parameters.json')
                )

def dump_json(obj, filename):
    with open(filename, 'w') as f:
        json.dump(obj, f, indent=2)
        f.write('\n')

if __name__ == '__main__':
    main()
